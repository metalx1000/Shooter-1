extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var horizontal = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
	var x = get_viewport_rect().size.x 
	x = remap(horizontal, -1, 1, 0, x)
	$Label.text = str(horizontal)
	$Label.text += "\n" + str(x)
