extends Sprite2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_pos()
	
func get_pos():
	#left and right
	var px = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
	var winx = get_viewport_rect().size.x 
	position.x = remap(px, -1, 1, 0, winx)
	
	#up and down
	var py = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	var winy = get_viewport_rect().size.y 
	position.y = remap(py, -1, 1, 0, winy)
